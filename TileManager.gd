extends Panel

onready var parent = get_parent()
onready var tiles = parent.tiles

func _on_AddTiles_pressed():
	var area_drawn_start = parent.area_drawn_start
	var area_drawn_end=parent.area_drawn_end
	var start_x
	var end_x
	if area_drawn_start.x < area_drawn_end.x:
		start_x = area_drawn_start.x
		end_x = area_drawn_end.x
	else:
		end_x = area_drawn_start.x
		start_x = area_drawn_end.x
	
	var start_y
	var end_y
	if area_drawn_start.y < area_drawn_end.y:
		start_y = area_drawn_start.y
		end_y = area_drawn_end.y
	else:
		end_y = area_drawn_start.y
		start_y = area_drawn_end.y
	
	for x in range(start_x,end_x+1):
		for y in range(start_y,end_y+1):
			create_tile(x,y)
	
	print(tiles)


func create_tile(x,y):
	if tiles[x][y] != null:
		return
	var tile = parent.base_tile.duplicate()
	tile.region_start_x = x*16
	tile.region_start_y = y*16
	tile.group = int($Inspector/HBoxContainer/Group.get_line_edit().text)
	tiles[x][y] = tile
