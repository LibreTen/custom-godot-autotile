extends ScrollContainer

var zoom_level

const colors = [Color(0,0,0,0),Color.red,Color.blue,Color.green,Color.yellow,Color.black]

onready var tiles : Array = get_parent().tiles
onready var button_group : ButtonGroup = get_node("../Colors/0").group

var color_selected = 0

func _input(event):
	if event is InputEventMouse:
		if Input.is_mouse_button_pressed(BUTTON_LEFT) || Input.is_mouse_button_pressed(BUTTON_RIGHT):
			var cursor_pos = get_local_mouse_position()
			cursor_pos+=Vector2(get_h_scroll(),get_v_scroll())
			zoom_level = get_parent().zoom_level
			
			var x = int((cursor_pos/zoom_level/16).x)
			var y = int((cursor_pos/zoom_level/16).y)
			
#			if x<0 || x>=tiles.size() || y<0 || y>=tiles[0].size():
#				return
			
			if !get_parent().is_mouse_over_tileset:
				return
			
			var subx = int((cursor_pos/zoom_level/16).x*3)%3
			var suby = int((cursor_pos/zoom_level/16).y*3)%3
			
			var group : int
			
			if Input.is_mouse_button_pressed(BUTTON_RIGHT):
				group = -1
			else:
				group = color_selected
				if get_parent().data.groups.size() <= group:
					get_parent().data.groups.resize(group+1)
				print(get_parent().data.groups)
				if get_parent().data.groups[group] == null:
					var g = get_parent().base_group.duplicate()
					g.icon_x = x
					g.icon_y = y
					get_parent().data.groups[group] = g
			
			var t = tiles[x][y]
			if t == null:
				t = get_parent().base_tile.duplicate()
			
			match subx:
				0:
					match suby:
						0:
							t["up_left"] = group
						1:
							t["left"] = group
						2:
							t["down_left"] = group
				1:
					match suby:
						0:
							t["up"] = group
						1:
							t["group"] = group
						2:
							t["down"] = group
				2:
					match suby:
						0:
							t["up_right"] = group
						1:
							t["right"] = group
						2:
							t["down_right"] = group
			tiles[x][y] = t
			$Control/TileSet/Masks.update()
			

func _on_Masks_draw():
	if tiles == null:
		return
	for x in tiles.size():
		if tiles[x] == null:
				return
		for y in tiles[0].size():
			var t = tiles[x][y]
			if t == null:
				continue
			draw_tile(x,y,"up_left",0,0,t)
			draw_tile(x,y,"left",0,1,t)
			draw_tile(x,y,"down_left",0,2,t)
			
			draw_tile(x,y,"up",1,0,t)
			draw_tile(x,y,"group",1,1,t)
			draw_tile(x,y,"down",1,2,t)
			
			draw_tile(x,y,"up_right",2,0,t)
			draw_tile(x,y,"right",2,1,t)
			draw_tile(x,y,"down_right",2,2,t)


func draw_tile(x,y,dir,offset_x,offset_y,t):
	var rect = Rect2(x*16+offset_x*16.0/3,y*16+offset_y*16.0/3,16.0/3,16.0/3)
	var group = t[dir]
	var color :Color = colors[group+1]
	color.a -= 0.6
	
	$Control/TileSet/Masks.draw_rect(rect,color,true)


func _on_color_pressed():
	color_selected = int(button_group.get_pressed_button().name)
