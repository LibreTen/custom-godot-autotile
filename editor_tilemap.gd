extends TileMap
tool

export(String, FILE, "*.tset") var tileset_file
export var disable_autotile = false

var tset : Dictionary
var texture_base :Texture

func _init():
	
	
	if tileset_file != "":
		print("initializing")
		load_file()
		start_tilemap()
		start_tileset()
		fix_invalid_tiles()


func _ready():
	if tileset_file != "":
		print("initializing")
		load_file()
		start_tilemap()
		start_tileset()
		fix_invalid_tiles()


func set_cell(x,y,tile,flip_x=false,flip_y=false,transpose=false,autotile_coord=Vector2.ZERO):
	.set_cell(x,y,tile,flip_x,flip_y,transpose,autotile_coord)
	update_cell(x,y)

func update_bitmask_area(pos):
	for x in range(pos.x-1,pos.x+2):
		for y in range(pos.y-1,pos.y+2):
			update_cell(x, y)


func update_bitmask_region(start:=Vector2.ZERO,end:=Vector2.ZERO):
	if (end.x < start.x || end.y < start.y) || (end.x == start.x && end.y == start.y):
		var a = get_used_cells()
		for i in range(a.size()):
			var vector = a[i]
			update_cell(vector.x, vector.y)
		
		return
	for x in range(start.x-1,end.x+2):
		for y in range(start.y-1,end.y+2):
			update_cell(x, y)
		

func update_cell(x,y):
	var cell = get_cell(x,y)
	if cell ==-1 || disable_autotile:
		return
	
	
	#print(is_wrong(x,y))
	
	fix(x,y)
	

#Cycle through every tile while looking for one that fits the list of constraints which
#includes at least up,down,left,right and diagonals
func fix(x,y):
	var directions = {}
	var tiles = tset.tiles
	var possible_values = []
	var low_priority = []
	
	var group = get_cell(x,y)
	
	for i in tiles.size():
		if tiles[i].group == group:
			possible_values.append(i)
	low_priority = possible_values.duplicate()
	
	directions["left"] = check_directions(x-1,y,tiles)
	directions["right"] = check_directions(x+1,y,tiles)
	directions["up"] = check_directions(x,y-1,tiles)
	directions["down"] = check_directions(x,y+1,tiles)
	directions["up_left"] = check_directions(x-1,y-1,tiles)
	directions["up_right"] = check_directions(x+1,y-1,tiles)
	directions["down_left"] = check_directions(x-1,y+1,tiles)
	directions["down_right"] = check_directions(x+1,y+1,tiles)
	
	print(directions)
	#print("dir:",directions)
	
	var values_to_be_removed = []
	
	for u in possible_values:
		for d in directions:
			if directions[d] != tiles[u][d]:# && tiles[u][d] != -1:
				values_to_be_removed.append(u)
				break
	
	for u in values_to_be_removed:
		possible_values.erase(u)
	
	if possible_values.empty():
		var highest_score = 0
		var highest_tile = low_priority[0]
		for u in low_priority:
			var score = 0
			for d in directions:
				if directions[d] == tiles[u][d]:
					if d in ["left","right","up","down"]:
						score += 500
					else:
						score += 10
				elif tiles[u][d] !=group:
					if d in ["left","right","up","down"]:
						score += 100
					else:
						score += 1
			if score > highest_score:
				highest_score = score
				highest_tile = u
		possible_values.append(highest_tile)
	
	
	print(possible_values)
	print(tiles[possible_values[0]])
	if possible_values.size() > 0:
		#var group = tiles[possible_values[0]]["group"]
		var coords_dict = tset["coords"]
		var x2 = coords_dict[possible_values[0]].x
		var y2 = coords_dict[possible_values[0]].y
		print("--")
		.set_cell(x,y,group,false,false,false,Vector2(x2,y2))

func check_directions(x,y,tiles):
	var cell = get_cell(x,y)
	return cell


func load_file():
	var file = File.new()
	file.open(tileset_file,File.READ)
	tset = parse_json(file.get_as_text())
	file.close()

func start_tilemap():
	cell_size.x = tset.cell_size_x
	cell_size.y = tset.cell_size_y

func start_tileset():
	texture_base = load("res://"+tset.texture)
	tile_set = TileSet.new()
	var i = 0
	for u in tset.groups:
		#print(i)
		#print(u)
		tile_set.create_tile(i)
		tile_set.tile_set_tile_mode(i,2)
		var tex = AtlasTexture.new()
		tex.atlas = texture_base
		
		tex.region.position.x = 0
		tex.region.position.y = 0
		var region_size_x = texture_base.get_width()
		var region_size_y = texture_base.get_height()
		
#		tex.region.position.x = u.region_start_x
#		tex.region.position.y = u.region_start_y
#		var region_size_x = u.region_size_x * tset.cell_size_x
#		var region_size_y = u.region_size_y * tset.cell_size_y
		tex.region.size = Vector2(region_size_x,region_size_y)
		tile_set.tile_set_texture(i,tex)
		tile_set.tile_set_region(i,Rect2(0,0,region_size_x,region_size_y))
		tile_set.autotile_set_size(i,Vector2(16,16))
		tile_set.autotile_set_icon_coordinate(i,Vector2(tset.groups[i].icon_x,tset.groups[i].icon_y))
		i+=1
