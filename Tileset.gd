extends Control

onready var scroll := $ScrollContainer
onready var texture := $ScrollContainer/Control/TileSet
onready var workspace := $ScrollContainer/Control
onready var pointer := $ScrollContainer/Control/TileSet/Pointer

const base_tile = {
	"region_start_x":0,
	"region_start_y":0,
	"group":-1,
	"up":-1,
	"down":-1,
	"left":-1,
	"right":-1,
	"up_left":-1,
	"up_right":-1,
	"down_left":-1,
	"down_right":-1
}

const base_group = {
	"icon_x":0,
	"icon_y":0
}

enum TOOL {
	SELECT,
	MASK
}

var zoom_level := 1.0
var zoom_multiplier = 0.1

var tiles = []

var tool_selected = TOOL.MASK
var tile_selected

var area_drawn_start :=Vector2.ZERO
var area_drawn_end := Vector2.ZERO
var tex_size

var is_mouse_over_tileset = false
var is_drawing = false

var data

func _ready():
	zoom_level = 7
	zoom(1)
	
	data = {
		"minimal":true,
		"texture":"dungeon_sheet_0.png",
		"cell_size_x":16,
		"cell_size_y":16,
		"groups":[],
		"coords":[],
		"tiles":[]
	}
	
	init_texture()
	

func init_texture():
	tex_size = texture.texture.get_size()

	tiles.resize(int(tex_size.x/16))
	var y_size = int(tex_size.y/16)
	
	for x in tiles.size():
		if tiles[x] == null:
			tiles[x]=[]
		tiles[x].resize(y_size)
	
	#$ScrollContainer.resize(tiles.size(),y_size)

func _input(event):
	if event.is_action_pressed("ui_scroll_down") && Input.is_action_pressed("ui_control"):
		zoom(1-zoom_multiplier)
		accept_event()
	elif event.is_action_pressed("ui_scroll_up") && Input.is_action_pressed("ui_control"):
		zoom(1+zoom_multiplier)
		accept_event()
	elif event.is_action_pressed("ui_left_button"):
		if tool_selected == TOOL.SELECT:
			if is_mouse_over_tileset:
				is_drawing = true
				set_area_drawn_start(tile_selected)
			
	elif event.is_action_released("ui_left_button"):
		if tool_selected == TOOL.SELECT && is_drawing:
			print(area_drawn_start,area_drawn_end)
			selected()
		is_drawing = false
	elif Input.is_action_pressed("ui_drag") && event is InputEventMouseMotion:
		var dragged = event.get_relative()
		scroll.set_h_scroll(scroll.get_h_scroll() - dragged.x * workspace.get_scale().x)
		scroll.set_v_scroll(scroll.get_v_scroll() - dragged.y * workspace.get_scale().x)

func _process(delta):
	#var cursor_pos = get_viewport().get_mouse_position()
	var cursor_pos = scroll.get_local_mouse_position()
	cursor_pos+=Vector2(scroll.get_h_scroll(),scroll.get_v_scroll())
	var x = floor((cursor_pos/zoom_level/16).x)
	var y = floor((cursor_pos/zoom_level/16).y)
	tile_selected = Vector2(x,y)
	if Input.is_action_pressed("ui_left_button"):
		set_area_drawn_end(tile_selected)
		pointer.update()

func zoom(amount):
	zoom_level*=amount
	texture.scale = Vector2(zoom_level,zoom_level)
	workspace.rect_min_size = texture.get_rect().size *zoom_level
	#$ScrollContainer/Control/SelectBox.scale = Vector2(zoom_level,zoom_level)


func set_tool(t):
	tool_selected = t
	

func selected():
	if !pointer.visible:
		return
	$InspectorPanel/Inspector.show()
	
	

func set_area_drawn_start(value : Vector2):
	if value.x < tex_size.x/16 && value.y < tex_size.y/16:
		pointer.show()
		area_drawn_start = value
		pointer.position = area_drawn_start*16

func set_area_drawn_end(value : Vector2):
	if is_drawing:
		if value.x < tex_size.x/16 && value.y < tex_size.y/16:
			area_drawn_end = value


func _on_Pointer_draw():
	var size = (area_drawn_end-area_drawn_start)*16
	var offset := Vector2.ZERO
	if area_drawn_end.x >= area_drawn_start.x:
		size+= Vector2.RIGHT*16
	else:
		offset.x = 16
		size+= Vector2.LEFT*16
	
	if area_drawn_end.y >= area_drawn_start.y:
		size+= Vector2.DOWN*16
	else:
		offset.y = 16
		size+= Vector2.UP*16
	var rect = Rect2(offset,size)
	pointer.draw_rect(rect,Color.orange,false)
	pointer.draw_rect(rect,Color(0.5,0.4,0,0.3),true,1)
	#pointer.draw_circle(Vector2.ZERO,1,Color.red)


func _on_TileSet_draw():
	
	var x_count = int(tex_size.x/16)
	var y_count = int(tex_size.y/16)
	var points = []
	for x in x_count:
		var start = Vector2(x*16,0)
		var end = Vector2(x*16,tex_size.y)
		points.append(start)
		points.append(end)
	for y in y_count:
		var start = Vector2(0,y*16)
		var end = Vector2(tex_size.x,y*16)
		points.append(start)
		points.append(end)
	texture.draw_multiline(points,Color(0.2,0.6,0.1,0.7))



func _on_Panel_mouse_entered():
	is_mouse_over_tileset = true



func _on_Panel_mouse_exited():
	is_mouse_over_tileset = false





func _on_Save_pressed():
	$SaveDialog.show()
	$SaveDialog.invalidate()


func _on_Load_pressed():
	$LoadDialog.show()
	$LoadDialog.invalidate()

func _on_SelectTexture_pressed():
	$TextureDialog.show()
	$TextureDialog.invalidate()


func _on_SaveDialog_file_selected(path):
	print(path)
	
	data.tiles = []
	data.coords = []
	
	for x in tiles.size():
		for y in tiles[x].size():
			if tiles[x][y] != null && tiles[x][y]["group"] != -1:
				data.tiles.append(tiles[x][y])
				data.coords.append({"x":x,"y":y})
	
	if !$HBoxContainer/CheckButton.pressed:
		data["minimal"] = true
#		for u in data.tiles:
#			if u.up_left == -1:
#				if u.up==-1 || u.left == -1:
#					u.up_left = -2
#			if u.up_right == -1:
#				if u.up==-1 || u.right == -1:
#					u.up_right = -2
#			if u.down_left == -1:
#				if u.down==-1 || u.left == -1:
#					u.down_left = -2
#			if u.down_right == -1:
#				if u.down==-1 || u.right == -1:
#					u.down_right = -2
	else:
		data["minimal"] = false
	
	
	var file = File.new()
	file.open(path,File.WRITE)
	file.store_string(to_json(data))


func _on_LoadDialog_file_selected(path:String):
	print(path)
	var file = File.new()
	var err = file.open(path,File.READ)
	if err != 0:
		error_opening_file("Error "+str(err))
		return
	data = parse_json(file.get_as_text())
	if data == null:
		error_opening_file("Not a tileset file")
		return
	
	
	var tex = load(path.get_base_dir()+"/"+data.texture)
	if !tex is Texture:
		error_opening_file("Failed to load tileset's texture")
		return
	
	for u in data.tiles:
		for v in u:
			if u[v] == -2:
				u[v] = -1
	
	if !data.minimal:
		$HBoxContainer/CheckButton.pressed = true
	
	tiles = []
	$ScrollContainer.tiles = tiles
	texture.texture = tex
	init_texture()
	
	for i in data.coords.size():
		var x = data.coords[i].x
		var y = data.coords[i].y
		tiles[x][y] = data.tiles[i]
	
	
	$ScrollContainer/Control/TileSet/Masks.update()

func _on_TextureDialog_file_selected(path:String):
	var tex = load(path)
	if !tex is Texture:
		error_opening_file("Failed to load texture")
		return
	
	texture.texture = tex
	print(path.get_file())
	data["texture"] = path.get_file()
	init_texture()


func error_opening_file(message):
	$Error.show()
	$Error.dialog_text = "Error opening file\n"+message
